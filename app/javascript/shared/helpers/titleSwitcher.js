export const changeTabTitle = () => {
  let currentTitle = document.title;
  // Store the current title, to avoid re-evaluation later.
  const originalTitle = currentTitle;
  const newTitle = "Nieuw bericht ontvangen!";

  const intervalId = setInterval(() => {
    if (document.title == originalTitle) {
      document.title = newTitle;
    }
    else {
      document.title = originalTitle;
    }
  }, 1000);

  document.addEventListener('visibilitychange', () => {
    if (document.visibilityState === 'visible') {
      clearInterval(intervalId)
      document.title = originalTitle;
    }
  });
};
