import { addClasses, removeClasses, toggleClass } from './DOMHelpers';
import { IFrameHelper } from './IFrameHelper';
import { isExpandedView } from './settingsHelper';

export const bubbleImg =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAYAAAAehFoBAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAQVSURBVFhHzZhdaBRXFMfvzFoVLSWaGLVtqPUDURNqUJFCLWlA2odStkXxQQUVrVoq+tDXqPiiT37gQ1tfjCC0hbYJbSlVqan4GVAXMTE+iOtiBRPj7sZkd7I7M3f83zvH3R0TJ7vrbGZ+cDnn3jtz73/O3K8Z5hX7/+lWHg1otZpuLuPcWm1ZVjNSk27wla2d0Ul0mb/oJm/KGmYLt6wIxCWQXonJeQqmDdfu6xscfoeaqDzotApR/BZWiCwbPMCfMJuoWe85fOWh8kzT1+B1R+0uPSOCNxWmbrwhFk+9i4j8RR1UiqPU3euBhpqQXMenV2B830K036euS4dzvgvtjIvYAkR/S0lC8Wi6sY5u9oNERjfrSYoDhawD3CCesAOpShb4Q0xRlDnk51DJ5oBYIfIIkp9iBe9BSxv5OUYINrj1NUyTnfOdcDprOLQ4hgQmGaKq3FcUNo2KfAcrR1dIVRso64wwxszuIIkVqIpSP6BlP6SsUzCextsdxyPemvyGGKaS3JCglSFi5wJHEm9fvvnCCAdloo1GVSpjLBdOTrBlsQ/IDSRTJoY+EjYf4SInW388yVoO/sA2frOX/fT7GSrNM5TS2MFjrWzDzr3s+9bfqDRPKq2xQ6hfv6OFHTvxM5UWhdwX8oItNpM8V3794192u+ceGxxKs1/az7HY/4+pxubcf52s80YXhKfZmfNXWefNbqqxOX/xOruGeiG849J1drbjGtW4gxEwS9jCCIfIcwWTkzybx7395NnEkwPk2Vick2cjHqSQTFYnb0zkApETbJjcINeVrz5vZm9OnSL9FY2LWf2iedJ/wbrwalZbM136SxbOZQ2L50v/BV989jGbUzdb+gvm1rHmVXIuFYE1TI4NIncEqWiePHU/yPX1u9f3PolbpmlSbmwQ0C1CZ35IYK0jWxQ1093PRjOq3etra6YxVS3s3h0cG54Km7ujK9bbSm4QSU4IqReE4zj8IPJRmBFn0ADQjp3uS+E43gm+iNvJDRqnyDpBhKvkCA8W0V2n/x71y0iiG+aPdGFQcPxsGaEcF4jpLU5tvo9lk1tnMdk+paxkxLqCwZ3EXla5X0hFAg2JR/FnWyk7NlnD3Ga/EV8Qu07pHxOmyQ/Y948rCQRrLUkonYxufEcNjQflRfZl0MhSfO89kE1WCGy9FxDZ8v+rjYaW0Y9T+14ioroHyf3wUS7J1HADGj8pevKAyOWe2NvUdOUZSA1vx0bTZpRyTswTMTn3b62/87CvGiLCELEZNiolvQKcbXsSQ1od3eo/WFlOkLbRiOCNlB3Z4k/QJRBS1bvkOsBKcxHmkwmh0AO7JCBkDSNM0cwBsUdhKrMSvC7xwXThMVUuW1QVXCBSTLwIVpFGKgo2WAkaIdjjIcDYcyW2o3cx5U/NAAAAAElFTkSuQmCC';

export const body = document.getElementsByTagName('body')[0];
export const widgetHolder = document.createElement('div');

export const bubbleHolder = document.createElement('div');
export const chatBubble = document.createElement('button');
export const closeBubble = (() => {
  const bubble = document.createElement('button');
  bubble.ariaLabel = "sluit chat";

  return bubble;
})()
export const notificationBubble = document.createElement('span');

export const setBubbleText = bubbleText => {
  if (isExpandedView(window.$chatwoot.type)) {
    const textNode = document.getElementById('woot-widget--expanded__text');
    textNode.innerHTML = bubbleText;
  }
};

export const createBubbleIcon = ({ className, src, target }) => {
  let bubbleClassName = `${className} woot-elements--${window.$chatwoot.position}`;
  const bubbleIcon = document.createElement('img');
  bubbleIcon.src = src;
  bubbleIcon.alt = 'open chat';
  target.appendChild(bubbleIcon);

  if (isExpandedView(window.$chatwoot.type)) {
    const textNode = document.createElement('div');
    textNode.id = 'woot-widget--expanded__text';
    textNode.innerHTML = '';
    target.appendChild(textNode);
    bubbleClassName += ' woot-widget--expanded';
  }

  target.className = bubbleClassName;
  return target;
};

export const createBubbleHolder = hideMessageBubble => {
  if (hideMessageBubble) {
    addClasses(bubbleHolder, 'woot-hidden');
  }
  addClasses(bubbleHolder, 'woot--bubble-holder');
  body.appendChild(bubbleHolder);
};

export const createNotificationBubble = () => {
  addClasses(notificationBubble, 'woot--notification');
  return notificationBubble;
};

export const onBubbleClick = (props = {}) => {
  const { toggleValue } = props;
  const { isOpen } = window.$chatwoot;
  if (isOpen !== toggleValue) {
    const newIsOpen = toggleValue === undefined ? !isOpen : toggleValue;
    window.$chatwoot.isOpen = newIsOpen;

    toggleClass(chatBubble, 'woot--hide');
    toggleClass(closeBubble, 'woot--hide');
    toggleClass(widgetHolder, 'woot--hide');
    IFrameHelper.events.onBubbleToggle(newIsOpen);

    if (!newIsOpen) {
      chatBubble.focus();
    }
  }
};

export const onClickChatBubble = () => {
  bubbleHolder.addEventListener('click', onBubbleClick);
};

export const addUnreadClass = () => {
  const holderEl = document.querySelector('.woot-widget-holder');
  addClasses(holderEl, 'has-unread-view');
};

export const removeUnreadClass = () => {
  const holderEl = document.querySelector('.woot-widget-holder');
  removeClasses(holderEl, 'has-unread-view');
};
