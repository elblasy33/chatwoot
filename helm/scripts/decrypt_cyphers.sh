#!/bin/bash

for yaml in `find -type f -iname "values*.yaml"`; do 
    echo "Decyphering $yaml"
    FILE=$(cat $yaml)
    KEY=$(echo ${DECRYPT_KEY} | base64 -d)
    /usr/bin/docker run --rm slamdev/crypter crypter decrypt -s "${KEY}" -v "${FILE}" > $yaml
done

echo "All Done!"
