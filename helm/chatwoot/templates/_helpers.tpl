{{/*
Expand the name of the chart.
*/}}
{{- define "chatwoot.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{- define "chatwoot.secretsName" -}}
{{- printf "%s-secrets" (include "chatwoot.name" .) -}}
{{- end }}

{{- define "chatwoot.railsName" -}}
{{- printf "%s-rails" (include "chatwoot.name" .) -}}
{{- end }}

{{- define "chatwoot.sidekiqName" -}}
{{- printf "%s-sidekiq" (include "chatwoot.name" .) -}}
{{- end }}

{{- define "chatwoot.redisName" -}}
{{- printf "%s-redis" ( .Release.Name ) -}}
{{- end }}

{{- define "chatwoot.authSecretName" -}}
{{- printf "%s-basic-auth" (include "chatwoot.secretsName" .) -}}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chatwoot.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "chatwoot.labels" -}}
helm.sh/chart: {{ include "chatwoot.chart" . }}
{{ include "chatwoot.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "chatwoot.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chatwoot.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
